/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal3.internal;

import java.util.logging.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import pk.labs.LabC.contracts.Animal;
/**
 *
 * @author st
 */
public class activator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        pk.labs.LabC.logger.Logger.get().log(horse.class.getName(), "registering 3");
        bc.registerService(Animal.class.getName(), new horse(), null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        pk.labs.LabC.logger.Logger.get().log(horse.class.getName(), "unregistering 3");
        ServiceReference ref = bc.getServiceReference(Animal.class.getName());
        bc.ungetService(ref);
    }
    
}
